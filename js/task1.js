function main()
{
    let btn = document.createElement('button'),
        parent = document.getElementById('root'),
        ggg = new GetIp()
    btn.className = 'btn btn-red'
    btn.innerText = 'Вычислить меня по IP'

    btn.addEventListener('click', async () => {
        try{
            await ggg.getIp()
            ggg.render(parent)
        }
        catch(e)
        {
            console.error(e)
        }
    })

    parent.insertAdjacentElement('afterbegin',btn)
}


class GetIp
{
    options = {
        method: 'get',
        headers: {'Content-type': 'application/json'}
    }
    elements = {
        list: document.createElement('ul')
    }
    resultInfo = {}
    async getIp()
    {
        let result = await this.makeRequest('https://api.ipify.org/?format=json',this.options)
       
        if(!result['ip'])
        {   
            throw new Error('Ошибка при получении ответа IP')
        }   

        this.resultInfo = await this.makeRequest(`http://ip-api.com/json/${result.ip}`,this.options)
        if(Object.keys(this.resultInfo).length <= 0)
        {
            throw new Error('Список данных пуст!')
        }

    }

    async makeRequest(uri,options)
    {
        let response = await fetch(uri,options)
        let result = await response.json()
        return result
    }

    render(parent)
    {
        let {list} = this.elements

        for(let key in this.resultInfo)
        {
            let line = document.createElement('li')
            line.innerText = `${key}: ${this.resultInfo[key]};`
            // console.log(key, this.resultInfo[key])

            list.append(line)
            // console.log( this.resultInfo)
        }
        
        parent.insertAdjacentElement('beforeend',list)
        setTimeout(() => {
            parent.insertAdjacentHTML("beforebegin",`<iframe src="https://coub.com/embed/2ajeyq?muted=false&autostart=false&originalSize=false&startWithHD=false" allowfullscreen frameborder="0" width="480" height="270" allow="autoplay"></iframe>`)
        },1000)

    }
}


main()