const options = {
    method: 'get',
    headers: {'Content-type': 'application/json'}
}

function mainFunctional()
{
    try
    {
        let btn = document.createElement('button'),
        parent = document.getElementById('root')
        btn.className = 'btn btn-red'
        btn.innerText = 'Вычислить тебя по IP'

        btn.addEventListener('click', async () => {
            let res = await getIp()

            if(!res['ip'])
            {
                throw new Error('Ошибка при получении ответа IP')            
            }

            let info = await getInfo(res.ip)
            if(Object.keys(info) <= 0)
            {
                throw new Error('Список данных пуст!')
            }

            let self = document.createElement('ul')
                self.className = 'ip-info__list'
            
            //рендер
            render({
                parent,
                self,
                info
            })
        })

        parent.insertAdjacentElement('afterbegin',btn)
    }
    catch(e)
    {
        console.error(e)
    }
    
}

async function getIp()
{
    url = 'https://api.ipify.org/?format=json'
    let result = await fetch(url,options)
    return result.json()
}

async function getInfo(ip)
{
    url = `http://ip-api.com/json/${ip}`
    let result = await fetch(url,options)
    return result.json()
}

function render({parent,self,info}){
    this.parent = parent
    this.self = self
    this.info = info

    if(Object.keys(info).length > 0)
    {
        for (let key in info)
        {
            let elem = document.createElement('li')
            elem.className = 'ip-info__element'
            elem.insertAdjacentHTML('afterbegin',`<span class="ip-info__key">${key}: </span>${info[key]}`)
            parent.insertAdjacentElement('beforeend',elem)
        }

        setTimeout(() => {
            parent.insertAdjacentHTML("beforebegin",`<iframe src="https://coub.com/embed/2ajeyq?muted=false&autostart=false&originalSize=false&startWithHD=false" allowfullscreen frameborder="0" width="480" height="270" allow="autoplay"></iframe>`)
        },1000)
    }
}

mainFunctional()